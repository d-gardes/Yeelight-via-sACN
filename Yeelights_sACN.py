#!/usr/bin/python3

import sacn, signal, json, sys
from src.DMXYeelight import DMXYeelight

"""
		ENFOIROS 2023
	La fabrique à rêves

Node SACN <=> Yeelight smart bulbs

		Dorian Gardes
	Technique, By Enfoiros
"""

# Inits
yeelights = []
sacn_receiver = sacn.sACNreceiver()

def graceful_stop(signum, frame):
	"""
	Finish program cleanly

	:param int signum:
	:param signal.FrameType frame:
	"""
	sacn_receiver.stop()
	print(f'\nClean shutdown using {signal.Signals(signum).name}')
	exit(0)

def sacn_packet_received(sacn_packet):
	"""
	Callback called on each sACN packet received

	:param sacn.DataPacket sacn_packet:  abstracted sACN packet received
	"""
	[light.update(sacn_packet) for light in yeelights if sacn_packet.universe == light.universe]

def sacn_timeout_detected(universe, changed):
	"""
	Callback called on each sACN timeout or connexion init

	:param int universe: Universe id
	:param str changed: Can be 'timeout' or 'available'
	"""
	print(f'Connection {changed} on universe {universe}')
	if changed == 'timeout':
		[light.lamp.turn_off(effect='smooth', duration=3000) for light in yeelights if universe == light.universe]


# Load config file
try:
	config_file = open("config.json", "r", encoding='utf-8')
	yeelights = [DMXYeelight.from_config(conf) for conf in json.load(config_file).get('lamps')]
	config_file.close()

except OSError as e:
	print(f'Error opening config.json\n\t{e.__class__.__name__}: {e.strerror}', file=sys.stderr)
	exit(0)

# Check config file load worked well
if len(yeelights) < 1:
	exit(0)

listening_universes = {light.universe for light in yeelights}  # List universes to listen

{sacn_receiver.register_listener('universe', sacn_packet_received, universe=universe) for universe in listening_universes}  # Register to universes packets
{sacn_receiver.register_listener('availability', sacn_timeout_detected) for universe in listening_universes}  # Register to sACN connexion events reporting

# Graceful stop setup using signals catch. Note: SIGKILL isn't catchable
signal.signal(signal.SIGHUP, graceful_stop)
signal.signal(signal.SIGINT, graceful_stop)
signal.signal(signal.SIGTERM, graceful_stop)

# Start sACN listening
sacn_receiver.start()

while True:
	pass
