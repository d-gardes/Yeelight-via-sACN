from yeelight import Bulb, SceneClass

def remap(x, out_min=0, out_max=100, in_min=0, in_max=255) -> int:
	"""
	Remap number from a range to another one (Used to convert DMX values to percentage)

	:param int x: Input number
	:param int out_min: Target range minimum value
	:param int out_max: Target range maximum value
	:param int in_min: Initial range minimum value
	:param int in_max: Initial range maximum value
	:rtype int
	:return: number remapped to new range and converted to int if needed
	"""
	return int((x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min)

class DMXValues:
	def __init__(self):
		self._dmx_values = [0, 0, 0, 0, 0, 1700, 0]  # Values meaning defined in Readme DMX Chart section

	def __eq__(self, other):
		"""
		Compare the two list with a 'equal' operator
		:param any other: Object to compare
		:return: True if equal. False Otherwise
		"""
		if type(other) != DMXValues:
			return False
		else:
			return self._dmx_values == other._dmx_values

	def __ne__(self, other):
		"""
		Compare the two list with a 'not equal' operator
		:param any other: Object to compare
		:return: True if equal. False Otherwise
		"""
		if type(other) != DMXValues:
			return False
		else:
			return self._dmx_values != other._dmx_values

	def __str__(self):
		return f'( DMXValues: Mode {self.mode}, Brightness {self.brightness} %, Red {self.red}, Green {self.green}, Blue {self.blue}, Color temperature {self.color_temp}K, Speed {self.speed} )'

	def from_packet(self, dmx_address, dmx_packet):
		"""
		Set values from raw DMX packet

		:param int dmx_address: DMX address of the light
		:param tuple dmx_packet: complete raw DMX packet
		:rtype: DMXValues
		:return: Self instance
		"""
		self._dmx_values = list(dmx_packet[dmx_address - 1: dmx_address + 6])
		self._dmx_values[1] = remap(self._dmx_values[1])  # Remapping brightness value to percentage
		self._dmx_values[5] = remap(self._dmx_values[5], 1700, 6500)  # Remapping color temperature value to Kelvin
		return self

	@property
	def mode(self) -> int:
		return self._dmx_values[0]

	@property
	def brightness(self) -> int:
		return self._dmx_values[1]

	@property
	def red(self) -> int:
		return self._dmx_values[2]

	@property
	def green(self) -> int:
		return self._dmx_values[3]

	@property
	def blue(self) -> int:
		return self._dmx_values[4]

	@property
	def color_temp(self) -> int:
		return self._dmx_values[5]

	@property
	def speed(self) -> int:
		return self._dmx_values[6]


class DMXYeelight:
	def __init__(self, ip, sacn_universe, dmx_channel):
		"""
		Object representing a DMX Yeelight smart bulb

		:param str ip: IP address of the smart bulb
		:param int sacn_universe: Universe id to listen
		:param int dmx_channel: DMX channel of the light
		"""

		self._dmx_values = DMXValues()
		self.universe = sacn_universe
		self.dmx_channel = dmx_channel

		self.lamp = Bulb(ip, effect="sudden")
		self.lamp.turn_off(effect='smooth', duration=2000)  # Duration in ms
		if int(self.lamp.get_properties(['music_on'])['music_on']) == 1:  # This is not supposed to happen but added for better reliability
			self.lamp.send_command('set_music', [0])  # Manual set_music off because ``stop_music`` method doesn't work
			print(f'Light {ip}: music mode recovered from invalid state')
		self.lamp.start_music()

	def __del__(self):
		"""
		Safe object destruction
		"""
		self.lamp.turn_off(effect='smooth', duration=2000)  # Duration in ms
		self.lamp.send_command('set_music', [0])  # Manual set_music off because ``stop_music`` method doesn't work

	@staticmethod
	def from_config(lamp_config):
		"""
		Create DMXYeelight from config dictionary
		Example:
			{
				"IP": "192.168.0.1",
				"Universe": 1,
				"DMXChannel": 1
			}

		:param dict lamp_config: Config of the object to create
		:rtype DMXYeelight
		:return: Object created from config
		"""
		return DMXYeelight(lamp_config.get("IP"), lamp_config.get("Universe"), lamp_config.get("DMXChannel"))

	def is_concerned(self, sacn_packet) -> bool:
		"""
		Check if the light is concerned by this packet

		:param sacn.DataPacket sacn_packet: DataPacket received by sACN receiver
		:rtype bool
		:return: True if it needs update. False otherwise
		"""
		return sacn_packet.universe == self.universe

	def is_need_update(self, sacn_packet) -> bool:
		"""
		Check if the light is concerned by this new parameters

		:param sacn.DataPacket sacn_packet: DataPacket received by sACN receiver
		:rtype bool
		:return: True if it needs update. False otherwise
		"""

		if self.is_concerned(sacn_packet):
			return self._dmx_values != DMXValues().from_packet(self.dmx_channel, sacn_packet.dmxData)
		else:
			return False

	def update(self, sacn_packet):
		"""
		Send the new parameters to the smart bulb

		:param sacn.DataPacket sacn_packet: DataPacket received by sACN receiver
		"""

		if self.is_need_update(sacn_packet):
			new_dmx_values = DMXValues().from_packet(self.dmx_channel, sacn_packet.dmxData)

			if new_dmx_values.mode <= 10:  # RGB mode
				self.lamp.set_scene(SceneClass.COLOR, new_dmx_values.red, new_dmx_values.green, new_dmx_values.blue, new_dmx_values.brightness)

			elif new_dmx_values.mode <= 20:  # Color temperature mode
				self.lamp.set_scene(SceneClass.CT, new_dmx_values.color_temp, new_dmx_values.brightness)

			self._dmx_values = new_dmx_values
