# sACN <=> Yeelight smart bulbs
Enfoiros 2023 - *La fabrique à rêves* <br>
Dorian Gardes - Technique, By Enfoiros 🎬

### 💡🔌🎛️ Yeelight smart bulb control through sACN 💡🔌🎛️

#### ⚠️ **WARNING** ⚠️ This project is in an early alpha stage ⚠️

#### 🚧 TODO 🚧

- Ajout d'un système de macros
- Script de configuration automatique des ampoules (avec ajout dans le fichier de conf)
- ConcertReliabilityTool (outil de redémarrage simple en conditions concerts)

#### 🚫 Known limitations 🚫

- sACN receiver is **unicast** only for now

#### Features

- Handle all Yeelight devices supported by [library](https://yeelight.readthedocs.io/en/latest/index.html)
- Receive [sACN](https://github.com/Hundemeier/sacn) packets ⚠️ unicast only ⚠️
- Config file handling
- Deal with multiple lights at the same time

#### DMX Chart

| **Index** |     **Name**      | **DMX Value** | **Function**                                                              |
|:---------:|:-----------------:|:-------------:|:--------------------------------------------------------------------------|
|     1     |  Mode selection   |    0 – 10     | RGB mode                                                                  |
|           |                   |    11 – 20    | Color temperature mode                                                    |
|           |                   |   21 – 255    | Unused (Macro selection in the future)                                    |
|     2     |    Brightness     |    0 – 255    | Speed linear 0 – 100 %                                                    |
|     3     |        Red        |    0 – 255    | Red linear 0 – 100 % <br/>RGB mode only                                   |
|     4     |       Green       |    0 – 255    | Green linear 0 – 100 % <br/>RGB mode only                                 |
|     5     |       Blue        |    0 – 255    | Blue linear 0 – 100 % <br/>RGB mode only                                  |
|     6     | Color Temperature |    0 – 255    | Color temperature linear 1700K – 6500K <br/>(Color temperature mode only) |
|     7     |       Speed       |    0 – 255    | Speed linear 0 – 100 % <br/>(Macro only and if applicable)                |

#### Installation

```BASH
pip install -r ./requirements.txt 
cp ./config.inc.json ./config.json
# Edit the config.json to fit your needs
python3 ./Yeelights_sACN.py
```